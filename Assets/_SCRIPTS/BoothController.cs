using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoothController : MonoBehaviour
{
    // initialize multi-display and hardcode resolution 
    void Start()
    {
        Debug.Log("displays connected: " + Display.displays.Length);

        // TODO move these resolutions to a configuration file
        
        // running windowed for now; assuming this will be easier on the multiplexing
        Screen.SetResolution(1920, 1080, FullScreenMode.Windowed);

        // Display.displays[0] is the primary, default display and is always ON, so start at index 1.
        // Check if additional displays are available and activate each.
        if (Display.displays.Length > 1)
        {
            Display.displays[1].Activate(1020, 1980, 60);
        }
        if (Display.displays.Length > 2)
        {
            Display.displays[2].Activate(1020, 1980, 60);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            Application.Quit();
        }
        
    }
}
