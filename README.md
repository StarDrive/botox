# README #

This repository is for the Botox activation. Current project schedule:

- QA Targeted for April 7, 2022
- Go-Live targeted for April 21, 2022

### Project Description ###

* Ultimately aiming at two Unity scenes: Botox Booth, and Botox Reg App
* Botox Booth displays outputs to three displays. Display 0 is the main display, Display 1 = Left touch screen, Display 2 = Right touch screen
* Botox Reg App will be designed to run on a MS Surface Pro tablet
* Other detailss TBD


### Who do I talk to? ###

* Dan DiCicco (daniel.dicicco@gmail.com) (Unity Contractor)
* Mike Sanders or Jon Fox 